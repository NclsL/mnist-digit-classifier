import random
import pdb
import numpy as np


def sigmoid(z):
      return 1 / (1 + np.exp(-z))


def dsigmoid(z):
      return sigmoid(z) * (1 - sigmoid(z))


#To avoid slowdown 'caused by dsigmoid in learning
# def cross_entropy_cost_function(a, y):
#       return np.sum(-y * np.log(a) - (1 - y) * np.log(1 - a))
def cross_entropy_delta(a, y):
      return a - y


class Network(object):
      def __init__(self, sizes):
            self.layers = len(sizes)
            self.sizes = sizes
            self.biases = [np.random.randn(n, 1) for n in sizes[1:]]
            self.weights = [np.random.randn(n, m)/np.sqrt(m) for m, n in zip(sizes[:-1], sizes[1:])]
            self.velocities = [np.zeros(w.shape) for w in self.weights]


      #Feedforward, vector of activations a' -> a = sigmoid(w*a+b)
      def feedforward(self, activation):
            for b, w in zip(self.biases, self.weights):
                  activation = sigmoid((w @ activation) + b)
            return activation


      #Instead of calculating forwarding for single elements x in mini_batch, take the entire mini_batch
      #and operate on it. 'Activations' now has an array whose columns are the vectors of mini_batch
      def matrix_feedforward(self, activation):
            zs = []
            activations = [activation]

            for b, w in zip(self.biases, self.weights):
                  z = w @ activation + b
                  zs.append(z)
                  activation = sigmoid(z)
                  activations.append(activation)
            return activations, zs


      #training_data is tuples of (x, y), containing greyscale values of the image and the desired output
      def train(self, training_data, epochs, mini_batch_size, learning_rate,
                test_data, regularization_param, training_set_size, no_imprv_in_n=False, momentum_coefficient=False):

            lr = learning_rate
            counter = 0
            #Early stopping, no-improvement-in-n epochs
            training_accuracies = []

            for i in range(epochs):
                  random.shuffle(training_data)
                  mini_batches = [training_data[j:j + mini_batch_size] for j in range(0, len(training_data), mini_batch_size)]

                  for mini_batch in mini_batches:
                        self.update_mini_batch(mini_batch, learning_rate, regularization_param, training_set_size, momentum_coefficient)

                  if test_data:
                        accuracy = self.evaluate(test_data)
                        training_accuracies.append(accuracy)
                        last_n_errors = [(len(test_data) - n) / 100 for n in training_accuracies[-no_imprv_in_n:]]

                        """checking the improvement by calculating gradients"""
                        #Mean of gradient greater than -0.001 -> not enough improvement in past n epochs
                        #-> stop training to avoid overfitting. -0.001 is just a parameter found via trial and error
                        #if (no_imprv_in_n and len(last_n_errors) >= no_imprv_in_n and np.mean(np.gradient(last_n_errors)) > -0.001):
                        #      print("Early stopping, no-improvement-in-{}. Curr.: {}.".format(no_imprv_in_n, i))
                        #      break

                        """checking the improvement with no-improvement-in-n, the right way?"""
                        if no_imprv_in_n and accuracy < max(training_accuracies):
                              counter += 1
                        if no_imprv_in_n and counter > no_imprv_in_n:
                              learning_rate /= 2
                              print("Early stopping, no-improvement-in-{}. Curr.: {}.".format(no_imprv_in_n, i))
                              break
                        """
                        instead of stopping, we could halve the learning rate every time no-improvement-in-n has been satisfied
                        and break when lr reaches 1/128 of its original value as mentioned in the book
                        # if learning_rate <= lr/128:
                        #       break
                        """

                        print("Epoch {}: {} / {}".format(i, accuracy, len(test_data)))
                  else:
                        print("Epoch {} complete".format(i))


      #Update all weights and biases after calculating gradient on a small batch of results
      #mini_batch is list of tuples (x, y)
      def update_mini_batch(self, mini_batch, learning_rate, reg_param, n, momentum_coefficient):
            #contains array for each layer in network
            nabla_b = [np.zeros(b.shape) for b in self.biases]
            nabla_w = [np.zeros(w.shape) for w in self.weights]

            #nabla_w is the gradient vector, see (7)
            #w,n -> w',n = w,n - learning_rate*(partial derivative w)(16) [Backpropagation!]) (26)
            #where cost_function = (||y(x) - a||^2)/2, where y(x) is the target and a is output of neural network
            #However, we use batches and take average of one mini_batch that'll be used to calculate w,n
            #nabla_w represents cost_function/partial derivative

            #x is now array of 784x10 greyscales from the mini_batch,
            #and y 10x10, has the respective correct answers
            x = np.array([np.ravel(tuple[0]) for tuple in mini_batch]).transpose()
            y = np.array([np.ravel(tuple[1]) for tuple in mini_batch]).transpose()

            nabla_b, nabla_w = self.backpropagation(x, y)

            if momentum_coefficient:
                  #Update velocities for momentum based gradient descent, seemed to reach decent accuracy faster, but
                  #reg param method was more accurate in the end, needs more testing with other hyperparams
                  self.velocities = [momentum_coefficient * v - learning_rate * n_w for v, n_w in zip(self.velocities, nabla_w)]
                  self.weights = [w + v for w, v in zip(self.weights, self.velocities)]
            else:
                  # Update weights with regularization parameter
                  self.weights = [(1 - reg_param*learning_rate / n) * w - (learning_rate / len(mini_batch) * n_w)
                            for w, n_w in zip(self.weights, nabla_w)]

            self.biases = [b - (learning_rate / len(mini_batch) * n_b)
                           for b, n_b in zip(self.biases, nabla_b)]


      def backpropagation(self, activation, y):
            nabla_b = [np.zeros(b.shape) for b in self.biases]
            nabla_w = [np.zeros(w.shape) for w in self.weights]

            activations, zs = self.matrix_feedforward(activation)

            #In the first output error:
            #delta = nabla_C * dsigmoid(z) (BP1 & BP4), where nabla_C is (activations - y) HADAMARD dsigmoid(z)
            #gradient of b = delta, gradient of nabla_w = activations * delta
            #as seen in (69, 70), averaging over all data is done when updating the weighs

            #now use cross_entropy instead to avoid dsigmoid and its learning slowdown
            delta = cross_entropy_delta(activations[-1], y)
            nabla_b[-1] = delta.sum(1).reshape(len(delta), 1) #reshape from 10x10 to 10x1 by summing them up
            nabla_w[-1] = delta @ activations[-2].transpose()

            #Propagate the error backwards (BP2) and store in array
            #Similar to what happens above except
            #instead of nabla_C use weights * previous error (delta)
            for layer in range(2, self.layers):
                  z = zs[-layer]
                  delta = (self.weights[-layer + 1].transpose() @ delta) * dsigmoid(z)
                  nabla_b[-layer] = delta.sum(1).reshape(len(delta), 1) #from 10x10 to 10x1
                  nabla_w[-layer] = (delta @ activations[-layer-1].transpose())
            return nabla_b, nabla_w


      def evaluate(self, test_data):
            test_results = [(np.argmax(self.feedforward(x)) ,y) for (x, y) in test_data]
            return sum(int(x == y) for (x, y) in test_results)
