from network import Network
import mnist_loader

def main():
    nn = Network([784, 30, 10])
    training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
    nn.train(training_data,
             epochs=100,
             mini_batch_size=7,
             learning_rate=0.1,
             test_data=test_data[:1000],
             regularization_param=5,
             training_set_size=len(training_data),
             no_imprv_in_n=10
             # momentum_coefficient=0.001 #Smaller value, more friction (1-this), avoiding overshooting in momentum based gradient descent
             )

if __name__ == "__main__":
    main()
